$(document).ready(() => {
    $("#troll").mouseover((event) => {
	  let xRange = getRange(event.pageX, $(document).width() - 250);
      let yRange = getRange(event.pageY, $(document).height() - 250);
      let newX = randomIntFromInterval(xRange.from, xRange.to);
      let newY = randomIntFromInterval(yRange.from, yRange.to);
      $("#troll").css({left: newX, top: newY});
    });
});




//make sure we send the troll to the opposite part of the screen to minize 
//overlapping
function getRange(position, total) {
	if (position > total / 2) {
  	return {from: 0, to : total / 2};
  } else {
    return {from: total / 2, to: total }
  }
}

function randomIntFromInterval(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}
